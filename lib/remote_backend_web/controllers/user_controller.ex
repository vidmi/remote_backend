defmodule RemoteBackendWeb.UserController do
  use RemoteBackendWeb, :controller

  alias RemoteBackend.UserManager

  action_fallback RemoteBackendWeb.FallbackController

  def index(conn, _params) do
    {users, timestamp} = UserManager.get_users_and_timestamp()
    render(conn, "index.json", users: users, timestamp: timestamp)
  end
end
