defmodule RemoteBackend.AccountsTest do
  use RemoteBackend.DataCase

  alias RemoteBackend.Accounts

  describe "users" do
    alias RemoteBackend.Accounts.User

    @valid_attrs %{points: 42}
    @update_attrs %{points: 43}
    @invalid_attrs_list [%{points: nil}, %{points: -1}, %{points: 101}]

    def create_user(attrs \\ %{}) do
      %User{}
      |> User.changeset(attrs)
      |> Repo.insert()
    end

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> create_user()

      user
    end

    def get_user!(id), do: Repo.get!(User, id)

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "list_max_two_users_with_more_than_points/1 returns max 2 users with correct points" do
      test_points = 10

      [10, 20, 30, 40, 50]
      |> Enum.each(fn points -> user_fixture(%{points: points}) end)

      result = Accounts.list_max_two_users_with_more_than_points(test_points)

      assert length(result) <= 2

      for u <- result do
        assert u.points > test_points
      end
    end

    test "update_user!/2 with valid data updates the user" do
      user = user_fixture()
      assert user = Accounts.update_user!(user, @update_attrs)
      assert user.points == 43
    end

    test "update_user!/2 with invalid data raises error" do
      user = user_fixture()

      for invalid_attrs <- @invalid_attrs_list do
        catch_error(Accounts.update_user!(user, invalid_attrs))
      end
    end

    test "update_users_random_points!/1 updates users" do
      users = [user_fixture(), user_fixture(), user_fixture()]

      test_generate_user_points = fn -> 28 end
      {:ok, _} = Accounts.update_users_random_points!(users, test_generate_user_points)

      user_points_after =
        users
        |> Enum.map(fn u -> get_user!(u.id) end)
        |> Enum.map(fn u -> u.points end)

      assert user_points_after == [28, 28, 28]
    end

    test "update_users_random_points!/1 raises error when transaction fails" do
      too_high_points = 99_999

      catch_error(
        Accounts.update_users_random_points!([user_fixture()], fn -> too_high_points end)
      )
    end
  end
end
