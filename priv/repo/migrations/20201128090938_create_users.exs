defmodule RemoteBackend.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def up do
    create table(:users) do
      add :points, :integer

      timestamps()
    end

    create constraint(
             "users",
             "points_must_be_between_0_and_100",
             check: "points >= 0 AND points <= 100"
           )
  end

  def down do
    drop table(:users)
  end
end
