defmodule RemoteBackend.UserManagerTest do
  use RemoteBackend.DataCase

  alias RemoteBackend.UserManager

  setup do
    {:ok, user_manager} = start_supervised(RemoteBackend.UserManager)
    {:ok, user_manager: user_manager}
  end

  test "get_users_and_timestamp/1 after genserver is initialized", %{user_manager: user_manager} do
    assert UserManager.get_users_and_timestamp(user_manager) == {[], nil}
    assert {[], timestamp} = UserManager.get_users_and_timestamp(user_manager)
    assert {:ok, _, _} = DateTime.from_iso8601(timestamp)
  end

  test "get_users_and_timestamp/1 after genserver has updated user points", %{
    user_manager: user_manager
  } do
    UserManager.get_users_and_timestamp(user_manager)
    Kernel.send(user_manager, :work)
    Process.sleep(200)

    assert {users, timestamp} = UserManager.get_users_and_timestamp(user_manager)
    assert is_list(users)
    assert {:ok, _, _} = DateTime.from_iso8601(timestamp)
  end

  test "handle_call(:get_users_and_timestamp, ...) returns previous query result and timestamp" do
    test_state = %{max_number: 1234, timestamp: "previous timestamp", users: "previous users"}
    result = UserManager.handle_call(:get_users_and_timestamp, self(), test_state)

    expected_reply = {test_state.users, test_state.timestamp}

    assert {:reply, ^expected_reply, %{max_number: _, timestamp: _, users: _}} = result
    {_, _, result_state} = result
    assert result_state.max_number == test_state.max_number
    assert result_state.timestamp != test_state.timestamp
    assert is_list(result_state.users)
  end

  test "handle_info(:work, state) sets correct new state" do
    test_state = %{max_number: 1234, timestamp: "timestamp", users: "users"}

    {:noreply, result} = UserManager.handle_info(:work, test_state)

    assert result.max_number != 1234
    assert %{timestamp: "timestamp", users: users} = result
  end
end
