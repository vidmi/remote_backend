./deps:
	mix deps.get

start: ./deps
	docker-compose up -d
	mix ecto.setup
	mix phx.server

run-tests: ./deps
	docker-compose up -d
	mix test --trace

run-checks: ./deps
	mix credo
	mix format --check-formatted
	mix dialyzer

clean:
	docker-compose down
