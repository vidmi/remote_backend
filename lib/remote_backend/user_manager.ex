defmodule RemoteBackend.UserManager do
  use GenServer
  require Logger

  alias RemoteBackend.Accounts
  alias RemoteBackend.Accounts.User

  @moduledoc false

  @max_number_min 0
  @max_number_max 100

  @update_user_points_every_ms 60_000

  # Client
  @spec start_link([]) :: GenServer.on_start()
  def start_link(opt) do
    GenServer.start_link(__MODULE__, nil, opt)
  end

  @spec get_users_and_timestamp(term()) :: {[%User{}], String.t()}
  def get_users_and_timestamp(name \\ __MODULE__) do
    {users, timestamp} = GenServer.call(name, :get_users_and_timestamp)
    {users, timestamp}
  end

  # Server callbacks
  @impl true
  def init(_args) do
    state = %{
      timestamp: nil,
      max_number: gen_max_number(),
      users: []
    }

    schedule_work()

    {:ok, state}
  end

  @impl true
  def handle_call(
        :get_users_and_timestamp,
        _from,
        %{max_number: max_number, timestamp: previous_timestamp, users: previous_users} = state
      ) do
    new_users = Accounts.list_max_two_users_with_more_than_points(max_number)
    new_timetamp = DateTime.utc_now() |> DateTime.to_string()

    new_state = %{state | timestamp: new_timetamp, users: new_users}

    {:reply, {previous_users, previous_timestamp}, new_state}
  end

  @impl true
  def handle_info(:work, state) do
    Accounts.list_users() |> Accounts.update_users_random_points!()

    schedule_work()

    {:noreply, %{state | max_number: gen_max_number()}}
  end

  # Private

  defp schedule_work do
    Process.send_after(self(), :work, @update_user_points_every_ms)
  end

  defp gen_max_number do
    Enum.random(@max_number_min..@max_number_max)
  end
end
