# RemoteBackend

- This is a toy Phoenix backend which uses a GenServer to randomly update users' points.
- It implements the requirements/description outlined in the assignment. Tests were also included.
- By making GET requests this backend's index you (yes, **you**!) can intereact with this mysterious backend.
- In order to start backend, see instructions below.

# To Start
- ` make start`
  - this will start postgres using docker-compose configured to use port host port `5432`
  - make sure to stop any other process bound to host port `5432` otherwise postgres will not be able to start
- now you can visit `localhost:4000` via your browser or make requests to the service via `curl localhost:4000`

# To Clean Up
- `make clean`

# To run tests
- `make run-tests`

# Additional Points
## "Tests? Why did you write tests, that wasn't in the requirement, what a waste of time"
 - I think testing is very important.
 - Code which is easy to test, is easy to reason about (imo).

## Why do I update all user points in a single transction? [link](https://gitlab.com/vidmi/remote_backend/-/blob/master/lib/remote_backend/accounts.ex#L38)
- Requirement said that **all** users should be updated, not some.
- By using the `update_user!/2` we make sure the function will raise an erorr if it makes. When this happens in the genserver
the genserver will re-initialize to a clean state. Let is crash, as they say.

## Why does [`update_users_random_points!\/2`](https://gitlab.com/vidmi/remote_backend/-/blob/master/lib/remote_backend/accounts.ex#L38) take a function which generates a random number, why not just stick `Enum.random(min..max)` in there an be done with it?
 - To make it possible to sanely test.
 
