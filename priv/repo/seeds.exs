# Seeds database with 100 users.

alias RemoteBackend.Accounts.User
alias RemoteBackend.Repo
num_seed_users = 100
seed_user = %User{points: 0}

for user <- List.duplicate(seed_user, num_seed_users) do
  Repo.insert!(user)
end
