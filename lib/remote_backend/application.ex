defmodule RemoteBackend.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      RemoteBackend.Repo,
      # Start the Telemetry supervisor
      RemoteBackendWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: RemoteBackend.PubSub},
      # Start the Endpoint (http/https)
      RemoteBackendWeb.Endpoint
      # Start a worker by calling: RemoteBackend.Worker.start_link(arg)
      # {RemoteBackend.Worker, arg}
    ]

    # To avoid preoblems with Ecto repo pid ownership, tests supervise their own UserManager GenServer
    children =
      case Application.get_env(:remote_backend, :env) do
        :test -> children
        _ -> children ++ [{RemoteBackend.UserManager, name: RemoteBackend.UserManager}]
      end

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: RemoteBackend.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    RemoteBackendWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
