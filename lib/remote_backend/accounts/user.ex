defmodule RemoteBackend.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  @points_min 0
  @points_max 100

  @moduledoc false

  schema "users" do
    field :points, :integer

    timestamps()
  end

  @doc false
  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:points])
    |> validate_required([:points])
    |> validate_number(:points,
      greater_than_or_equal_to: @points_min,
      less_than_or_equal_to: @points_max
    )
  end

  @spec generate_random_points() :: :integer
  def generate_random_points(), do: Enum.random(@points_min..@points_max)
end
