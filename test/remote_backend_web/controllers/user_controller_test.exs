defmodule RemoteBackendWeb.UserControllerTest do
  use RemoteBackendWeb.ConnCase

  setup %{conn: conn} do
    start_supervised({RemoteBackend.UserManager, [name: RemoteBackend.UserManager]})
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  defp assert_valid_response(json_response) do
    assert Map.keys(json_response) == ["timestamp", "users"]

    for u <- json_response["users"] do
      assert Map.keys(u) == ["id", "points"]
    end
  end

  test "GET /", %{conn: conn} do
    for _ <- 1..2 do
      conn = get(conn, Routes.user_path(conn, :index))

      json_response(conn, 200)
      |> assert_valid_response()
    end

    Kernel.send(RemoteBackend.UserManager, :work)
    Process.sleep(300)

    for _ <- 1..2 do
      conn = get(conn, Routes.user_path(conn, :index))

      json_response(conn, 200)
      |> assert_valid_response()
    end
  end
end
