defmodule RemoteBackend.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias RemoteBackend.Repo

  alias RemoteBackend.Accounts.User

  # @points_max 100
  # @points_min 0

  @spec list_users() :: [%User{}]
  def list_users do
    Repo.all(User)
  end

  @spec list_max_two_users_with_more_than_points(:interger) :: [%User{}]
  def list_max_two_users_with_more_than_points(points) do
    query =
      from u in User,
        where: u.points > ^points,
        select: u,
        limit: 2

    Repo.all(query)
  end

  @spec update_user!(%User{}, map()) :: Ecto.Schema.t()
  def update_user!(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update!()
  end

  @spec update_users_random_points!([%User{}], (() -> integer)) :: :ok
  def update_users_random_points!(users, gen_points_fn \\ &User.generate_random_points/0) do
    Repo.transaction(fn ->
      users
      |> Enum.each(fn u -> update_user!(u, %{points: gen_points_fn.()}) end)
    end)
  end
end
